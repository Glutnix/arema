<?php

// connect to mysql
$db = new PDO('mysql:host=localhost;dbname=arema;charset=utf8', 'root', '');

// throw exceptions when we have sql errors
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// do not emulate preparation of statements
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

// =======

// create a statement, bind some parameters
try {
	$statement = $db->prepare("SELECT * FROM pages WHERE slug = :slug;");
	$statement->bindValue(":slug", $_GET['slug'], PDO::PARAM_STR);

// run the statement on the db server
	$statement->execute();

// fetch all the records!
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $ex) {
	echo "<pre>";
	print_r($ex);
	echo "</pre>";
	throw ($ex);
}

// =======

//var_dump($results);
echo count($results) . " result(s) found.";
echo "<pre>";
foreach ($results as $record) {
	echo "<hr />";
	var_dump($record);
}

echo "</pre>";