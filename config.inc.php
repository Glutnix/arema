<?php

if ($_SERVER['HTTP_HOST'] === "localhost" || $_SERVER['HTTP_HOST'] === "workspace.dev") {
	define("HOST_TYPE", "dev");
} else {
	define("HOST_TYPE", "live");
}

switch (HOST_TYPE) {
	case "dev":
		error_reporting(E_ALL);
		define("DB_DSN", 'mysql:host=localhost;dbname=arema;charset=utf8');
		define("DB_USERNAME", 'root');
		define("DB_PASSWORD", '');
		break;
	case "live": // no break
	default:
		error_reporting(E_ALL);
		ini_set('display_errors', false);
		ini_set("log_errors", 1);
		ini_set("error_log", "./php-error.log");
		define("DB_DSN", 'mysql:host=localhost;dbname=brettt_arema;charset=utf8');
		define("DB_USERNAME", 'brettt_arema');
		define("DB_PASSWORD", 'arema');
	// 
}