<?php

require "config.inc.php";

session_start();
session_regenerate_id(true);

require "lib/http_response_code.inc.php"; 
require "lib/password.php"; 

function arema_autoload($className) {
	require ("classes/" . $className . ".php");
}

spl_autoload_register("arema_autoload");

// DATABASE
try {
	// connect to mysql
	$db = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
	// throw exceptions when we have sql errors
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// do not emulate preparation of statements
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch (PDOException $ex) {
	if (HOST_TYPE === "dev") {
		echo "<pre>";
		print_r($ex);
		echo "</pre>";
		throw $ex;
	}
}

// GET CURRENT USER
if (isset($_SESSION['user']) && isset($_SESSION['user']['id'])) {
	$usermodel = new UserModel($db, $_SESSION['user']['id']);
} else {
	$usermodel = new UserModel($db);
}

// CONTROLLER

if (isset($_GET['page'])) {
	$requestedPage = $_GET['page'];
} else {
	$requestedPage = "";
}

switch ($requestedPage) {
	case "":
	case "home":
		$page = new HomePageView();
		break;
	case "contact":
		$model = new ContactFormModel($_POST);
		$page = new ContactFormView($model);
		break;
	case "product":
		$product_id = 0;
		if (isset($_GET['id'])) {
			$product_id = (int)$_GET['id'];
		}
		// ACTION MODE
		if (isset($_GET['action']) && $_GET['action'] === "add") {
			$mode = "blank"; // load blank form
		} else if (isset($_POST['action']) && $_POST['action'] === "add") {
			$mode = "create"; // validate and insert
		} else if (isset($_POST['delete'])) {
			$mode = "delete";
		} else if (isset($_GET['action']) && $_GET['action'] === "edit") {
			$mode = "edit"; // load unsubmitted form
		} else if (isset($_POST['action']) && $_POST['action'] === "edit") {
			$mode = "update"; // validate and update
		} else {
			$mode = "view"; // show team member page
		}

		// PERMISSIONS
		if ($mode !== "view") {
			// check if they are admin
			if (!$usermodel->is("admin")) {
				// throw security error
				$page = new NoAccessPageView();
				break;
			}
		}

		// MODEL
		if ($mode === "create") {
			$model = new ProductModel($db, $_POST);
			if ($model->id > 0) {
				// insert succeeded, redirect!
				header("Location: ./?page=product&id=" . $model->id);
				exit;
			}
		} else if ($mode === "update") {
			$model = new ProductModel($db, $product_id);
			$model->processInput($_POST);
			if ($model->validateData()) {
				// validation success, commit and redirect;
				$model->commit();
				header("Location: ./?page=product&id=" . $model->id);
				exit;
			}
		} else if ($mode === "delete") {
			$model = new ProductModel($db, $product_id);
			$model->remove();
			header("Location: ./?page=products");
			exit;
		} else if ($mode === "blank" || $mode === "view" || $mode === "edit") {
			$model = new ProductModel($db, $product_id);
		}
		
		// VIEW
		if ($mode === "create" || $mode === "blank" || $mode === "edit" || $mode === "update") {
			$categories = CategoryModel::fetchAll($db);
			$page = new ManageProductFormView($model, $categories);
		} else if ($mode === "view") {
			if (isset($model->data['id']) && $model->data['id'] > 0) {
				$page = new SingleProductPageView($model);
			} else {
				// couldn't find the team member
				$page = new NotFoundPageView();
			}
		}
		break;

	case "products":
		$collection = ProductModel::fetchAll($db); // call the factory
		// echo "<pre>"; print_r($collection); echo "</pre>";
		$page = new ProductsPageView($collection);
		break;

	case "team":
		$p = 1;
		if (isset($_GET['p'])) {
			$p = $_GET['p'];	
		}
		$limit = 6;
		$offset = ($p - 1) * $limit;
		$result = TeamMemberModel::fetchLimit($db, $limit, $offset);
		$page = new TeamPageView($result);
		break;

	case "team.json":
		$result = TeamMemberModel::fetchAll($db);
		$output = [];
		foreach ($result as $teammember) {
			array_push($output, $teammember->data); 
		}
		header("Content-Type: application/json");
		echo json_encode($output);
		exit;
		break;

	case "teammember":
		$teammember_id = 0;
		if (isset($_GET['id'])) {
			$teammember_id = (int)$_GET['id'];
		}
		// ACTION MODE
		if (isset($_GET['action']) && $_GET['action'] === "add") {
			$mode = "blank"; // load blank form
		} else if (isset($_POST['action']) && $_POST['action'] === "add") {
			$mode = "create"; // validate and insert
		} else if (isset($_POST['delete'])) {
			$mode = "delete";
		} else if (isset($_GET['action']) && $_GET['action'] === "edit") {
			$mode = "edit"; // load unsubmitted form
		} else if (isset($_POST['action']) && $_POST['action'] === "edit") {
			$mode = "update"; // validate and update
		} else {
			$mode = "view"; // show team member page
		}

		// PERMISSIONS
		if ($mode !== "view") {
			// check if they are admin
			if (!$usermodel->is("admin")) {
				// throw security error
				$page = new NoAccessPageView();
				break;
			}
		}

		// MODEL
		if ($mode === "create") {

			$photo = new Image;
			$photo->importUpload('photo', "img/team/", null, true );
			$data = array_merge($_POST, array('photo' => $photo));

			$model = new TeamMemberModel($db, $data);
			if ($model->id > 0) {
				// insert succeeded, redirect!
				header("Location: ./?page=teammember&id=" . $model->id);
				exit;
			}
		} else if ($mode === "update") {
			$photo = new Image;

			if (isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
				$photo->importUpload('photo', "img/team/", null, true );
				$data = array_merge($_POST, array('photo' => $photo));
			} else if (isset($_POST['removephoto'])) {
				// remove photo if checkbox applied.
				$data = $_POST;
				$data['photo'] = new Image();
			} else {
				$data = $_POST;
			}			

			$model = new TeamMemberModel($db, $teammember_id);
			$model->processInput($data);
			if ($model->validateData()) {
				// validation success, commit and redirect;
				$model->commit();
				header("Location: ./?page=teammember&id=" . $model->id);
				exit;
			}
		} else if ($mode === "delete") {
			$model = new TeamMemberModel($db, $teammember_id);
			$model->remove();
			header("Location: ./?page=team");
			exit;
		} else if ($mode === "blank" || $mode === "view" || $mode === "edit") {
			$model = new TeamMemberModel($db, $teammember_id);
		}
		
		// VIEW
		if ($mode === "create" || $mode === "blank" || $mode === "edit" || $mode === "update") {
			$rolesCollection = RoleModel::fetchAll($db);
			$page = new ManageTeamMemberFormView($model, $rolesCollection);
		} else if ($mode === "view") {
			if (isset($model->data['id']) && $model->data['id'] > 0) {
				$page = new SingleTeamMemberPageView($model);
			} else {
				// couldn't find the team member
				$page = new NotFoundPageView();
			}
		}
		break;

	case "category":
		$category_id = 0;
		if (isset($_GET['id'])) {
			$category_id = (int)$_GET['id'];
		}
		// ACTION MODE
		if (isset($_GET['action']) && $_GET['action'] === "add") {
			$mode = "blank"; // load blank form
		} else if (isset($_POST['action']) && $_POST['action'] === "add") {
			$mode = "create"; // validate and insert
		} else if (isset($_POST['delete'])) {
			$mode = "delete";
		} else if (isset($_GET['action']) && $_GET['action'] === "edit") {
			$mode = "edit"; // load unsubmitted form
		} else if (isset($_POST['action']) && $_POST['action'] === "edit") {
			$mode = "update"; // validate and update
		} else {
			$mode = "view"; // show team member page
		}

		// PERMISSIONS
		if ($mode !== "view") {
			// check if they are admin
			if (!$usermodel->is("admin")) {
				// throw security error
				$page = new NoAccessPageView();
				break;
			}
		}

		// MODEL
		if ($mode === "create") {
			$model = new CategoryModel($db, $_POST);
			if ($model->id > 0) {
				// insert succeeded, redirect!
				header("Location: ./?page=category&id=" . $model->id);
				exit;
			}
		} else if ($mode === "update") {
			$model = new CategoryModel($db, $category_id);
			$model->processInput($_POST);
			if ($model->validateData()) {
				// validation success, commit and redirect;
				$model->commit();
				header("Location: ./?page=category&id=" . $model->id);
				exit;
			}
		} else if ($mode === "delete") {
			$model = new CategoryModel($db, $category_id);
			$model->remove();
			header("Location: ./?page=categories");
			exit;
		} else if ($mode === "blank" || $mode === "view" || $mode === "edit") {
			$model = new CategoryModel($db, $category_id);
		}
		
		// VIEW
		if ($mode === "create" || $mode === "blank" || $mode === "edit" || $mode === "update") {
			$page = new ManageCategoryFormView($model);
		} else if ($mode === "view") {
			if (isset($model->data['id']) && $model->data['id'] > 0) {
				$page = new SingleCategoryPageView($model);
			} else {
				// couldn't find the team member
				$page = new NotFoundPageView();
			}
		}
		break;

	case "categories":
		$collection = CategoryModel::fetchAll($db); // call the factory
		// echo "<pre>"; print_r($collection); echo "</pre>";
		$page = new CategoriesPageView($collection);
		break;


	case "register":
		// get a blank user
		if (isset($_POST['action']) && $_POST['action'] === "add") {
			$model = new UserModel($db, $_POST);
			if ($model->id > 0) {
				// user register success
				$_SESSION['flash'] = "User '".$model->data['username']."' successfully created. Log in.";
				header("Location: ./");
				exit;
			}
		} else {
			$model = new UserModel($db);
		}
		// get the register view
		$page = new UserRegisterFormView($model);
		break;

	case "login":
		if (isset($_POST['username'])) {
			$un = $_POST['username'];
			$pw = $_POST['password'];
			$model = UserModel::authenticateUser($db, $un, $pw);
			if ($model->id > 0) {
				$_SESSION['user'] = array(
					'id' => $model->id,
					'username' => $model->data['username'],
					'role' => $model->data['role']
				);
				// login success! Redirect to place.
				$_SESSION['flash'] = "Welcome back ".$model->data['username']."!";
				header("Location: ./");
				exit;
			}
			// return useful error data
			$model->data['username'] = $_POST['username'];
			$model->errors['username'] = " ";
			$model->errors['password'] = "The username or password you entered is incorrect.";
		} else {
			$model = new UserModel($db);
		}
		$page = new LoginFormView($model);
		break;

	case "logout":
		session_destroy();
		header("Location: ./?page=login");
		exit;
		break;

	case "profile":
		// PERMISSIONS
		// check if they are user
		if (!$usermodel->is("user")) {
			// throw security error
			$page = new NoAccessPageView();
			break;
		}

		// get current user model
		$model = $usermodel;
		$model->errors['oldpassword'] = "";

		if (isset($_POST['email'])) {
			$result = $model->changeEmail($_POST['email']);
			if ($result) {
				$_SESSION['flash'] = "Email changed.";
			}
		}

		// page
		$page = new ManageUserProfileFormView($model);
		break;

	case "password":
		if (!isset($usermodel)) {
			$_SESSION['flash'] = "You're not logged in.";
			header("Location: ./?page=login");
			exit;
		}
		// get current user model
		$model = $usermodel;
		$model->errors['oldpassword'] = "";

		if (isset($_POST['oldpassword'])) {
			$result = $model->changePassword(
				$_POST['oldpassword'],
				$_POST['password'],
				$_POST['password2']
			);
			if ($result) {
				$_SESSION['flash'] = "Password changed.";
				header("Location: ./?page=profile");
				exit;
			}
		}

		// page
		$page = new ManageUserProfileFormView($model);
		break;

	case "about":
		$page = new AboutPageView();
		break;

	default:
		$page = new NotFoundPageView();
}

if (isset($usermodel)) {
	$page->setUser($usermodel);
}
if (isset($_SESSION['flash'])) {
	$page->setFlash($_SESSION['flash']);
}
$page->render();

$_SESSION['flash'] = null;
