<ol class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=team">Team</a></li>
  <li class="active"><?php $this->ee($this->data['name']); ?></li>
</ol>

<div id="container">
	<div class="row">
		<div class="col-sm-6">
			<?php $this->getPhotoElement(); ?>
		</div>
		<div class="col-sm-6">
			<h1><?php $this->ee($this->data['name']); ?></h1>
			<p><?php $this->ee($this->data['title']); ?></p>
			<p>
				<?php foreach ($this->model->roles as $role): ?>
					<span class="badge"><?php $this->ee($role->data['rolename']); ?></span>
				<?php endforeach; ?>
			</p>
			<p>
				<a href="?page=teammember&amp;action=edit&amp;id=<?php $this->ee($this->data['id']); ?>">
				<span class="glyphicon glyphicon-pencil"></span> 
				Edit <?php $this->ee($this->data['name']); ?></a>
			</p>

		</div>
</div>