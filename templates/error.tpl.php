<div class='jumbotron'>
    <h1>500 Error. My bad.</h1>
    <?php if (HOST_TYPE === 'dev'): ?>
    	<p class="lead"><?php echo $error_message; ?></p>
	<?php endif; ?>
    <p>Refresh the page, go back, or try again later.</p>
</div>
