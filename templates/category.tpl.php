<ol class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=categories">Products</a></li>
  <li class="active"><?php $this->ee($this->data['name']); ?></li>
</ol>


<h1><?php $this->ee($this->data['name']); ?></h1>
<p><?php $this->ee($this->data['description']); ?></p>

<ul>
	<?php foreach ($this->products as $product): ?>
	<li>
		<a href="?page=product&amp;id=<?php echo $product->id; ?>">
			<?php $this->ee($product->data['title']); ?>
		</a> - <?php $this->ee($product->data['price']); ?>

	</li>
	<?php endforeach; ?>
</ul>
<p>
	<a href="?page=category&amp;action=edit&amp;id=<?php $this->ee($this->data['id']); ?>">
	<span class="glyphicon glyphicon-pencil"></span> 
	Edit <?php $this->ee($this->data['name']); ?> Category</a>
</p>
