    <form method="POST" action="./?page=contact">

        <div class="form-group <?php $this->echoIfError('fullname', "has-error has-feedback"); ?>">
            <label for="fullname">Full Name</label>
            <input id="fullname" name="fullname" class="form-control"
                    value="<?php $this->ee($this->data['fullname']); ?>" 
                    placeholder="e.g. Joe Bloggs"
            />
            <?php $this->showError('fullname'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('email', "has-error has-feedback"); ?>">
            <label for="email">Email Address</label>
            <input id="email" name="email" type="email" class="form-control"
                    value="<?php $this->ee($this->data['email']); ?>" />
            <?php $this->showError('email'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('message', "has-error has-feedback"); ?>">
            <label for="message">Message</label>
            <textarea id="message" name="message" class="form-control"><?php $this->ee($this->data['message']); ?></textarea>
            <?php $this->showError('message'); ?>
        </div>
        
    	<div class="form-group">
    		<button class="btn btn-primary">Submit</button>
    	</div>
    </form>
