<?php
    $url = "./?page=teammember";
    if ($this->data['id'] > 0) {
        $url .= "&amp;id=" . $this->data['id'];
    }
?>
    <form method="POST" action="<?php echo $url;?>" enctype="multipart/form-data">

        <div class="form-group <?php $this->echoIfError('name', "has-error has-feedback"); ?>">
            <label for="name">Full Name</label>
            <input id="name" name="name" class="form-control"
                    value="<?php $this->ee($this->data['name']); ?>" 
                    placeholder="e.g. Joe Bloggs"
            />
            <?php $this->showError('name'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('title', "has-error has-feedback"); ?>">
            <label for="title">Job Title</label>
            <input id="title" name="title" class="form-control"
                    value="<?php $this->ee($this->data['title']); ?>" 
                    placeholder="e.g. Joe Bloggs"
            />
            <?php $this->showError('title'); ?>
        </div>


        <div class="form-group <?php $this->echoIfError('roleid', "has-error has-feedback"); ?>">
            <label for="roleid">Roles</label>
            <?php foreach ($this->roles as $role): ?>
                <?php
                    // figure out if this checkbox should be already checked
                    $checked = false;
                    foreach ($this->model->roles as $memberRole) {
                        if ($role->id === $memberRole->id) {
                            $checked = true;
                            break; // end foreach early
                        }
                    }
                ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"
                            name="roleid[]"
                            <?php if ($checked) echo 'checked="checked"'; ?>
                            value="<?php echo $role->id; ?>"> <?php $this->ee($role->data['rolename']); ?>
                    </label>
                </div>
            <?php endforeach; ?>
            <?php $this->showError('roleid'); ?>
        </div>



        <div class="form-group <?php $this->echoIfError('photo', "has-error has-feedback"); ?>">
            <label for="photo">Photo</label>
            <input id="photo" name="photo" class="form-control" type="file" />

            <p class="help-block">File must be a JPG, PNG or GIF smaller than 
                <?php echo min( ini_get( 'post_max_size' ), ini_get( 'upload_max_filesize' ) ); ?>B.
            </p>

            <?php if ($this->data['id'] > 0 && $this->data['photo']->filename !== ""): ?>
                <img src="<?php echo $this->data['photo']->filename; ?>" alt="" width="203" />
                <label>
                    <input id="removephoto" name="removephoto" type="checkbox">
                    Remove existing image
                </label>
            <?php endif; ?>

            <?php $this->showError('photo'); ?>
        </div>
        
    	<div class="form-group">
            
            <?php if ($this->data['id'] > 0): ?>
                <input type="hidden" name="action" value="edit" />
                <button class="btn btn-primary" type="submit">
                    <span class="glyphicon glyphicon-ok"></span>
                    Save Changes
                </button>
                <button class="btn btn-link pull-right" 
                    style="color: red;" type="submit" name="delete">
                    <span class="glyphicon glyphicon-trash"></span>
                    Delete
                </button>
            <?php else: ?>
                <input type="hidden" name="action" value="add" />
                <button class="btn btn-primary" type="submit">
                    <span class="glyphicon glyphicon-ok"></span>
                    Create Team Member
                </button>
            <?php endif; ?>

    	</div>
    </form>
