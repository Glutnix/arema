    <form method="POST" action="./?page=register">

        <div class="form-group <?php $this->echoIfError('username', "has-error has-feedback"); ?>">
            <label for="username">Username</label>
            <input id="username" name="username" class="form-control"
                value="<?php $this->ee($this->data['username']); ?>"
            />
            <?php $this->showError('username'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('email', "has-error has-feedback"); ?>">
            <label for="email">Email Address</label>
            <input id="email" name="email" type="email" class="form-control"
                value="<?php $this->ee($this->data['email']); ?>" />
            <?php $this->showError('email'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password', "has-error has-feedback"); ?>">
            <label for="password">Password</label>
            <input id="password" name="password" type="password" class="form-control"
                value="<?php $this->ee($this->data['password']); ?>" />
            <?php $this->showError('password'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password2', "has-error has-feedback"); ?>">
            <label for="password2">Confirm Password</label>
            <input id="password2" name="password2" type="password" class="form-control"
                value="<?php $this->ee($this->data['password2']); ?>" />
            <?php $this->showError('password2'); ?>
        </div>
        
    	<div class="form-group">
            <input type="hidden" name="action" value="add" />
    		<button class="btn btn-primary">
                <span class="glyphicon glyphicon-user"></span> Sign Up
            </button>
    	</div>
    </form>
