<ol class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li class="active">Products</li>
</ol>

<h1>Products</h1>
<ul>
<?php foreach($this->categories as $category): ?>
<li>
    <a href="?page=category&amp;id=<?php echo $category->data['id']; ?>">
        <?php $this->ee($category->data['name']); ?></a>
        - <?php $this->ee($category->data['productcount']); ?> 
        <?php $this->plural($category->data['productcount'], "item", "items"); ?>
</li>
<?php endforeach; ?>
</ul>
<p><a href="?page=category&amp;action=add">
	<span class="glyphicon glyphicon-plus"></span> Add Category</a></p>
<p><a href="?page=product&amp;action=add">
	<span class="glyphicon glyphicon-plus"></span> Add Product</a></p>
