<ol class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li class="active">Team</li>
</ol>

<div class="container">
	<?php $column_count = 1; ?>
	<?php foreach($this->team as $member): ?>

		<?php if ($column_count === 1): ?>

	<div class="row">
		<?php endif; ?>

		<div class="col-sm-4 clearfix">
			<a href="?page=teammember&amp;id=<?php echo $member->data['id']; ?>">
				<?php $this->getPhotoElement($member); ?>
			</a>
	    	<h2><a href="?page=teammember&amp;id=<?php echo $member->data['id']; ?>">
		        <?php $this->ee($member->data['name']); ?></a></h2>
		    <p><?php $this->ee($member->data['title']); ?></p>
		</div>

		<?php if ($column_count === 3): ?>

	</div>
		<?php endif; ?>

		<?php
			$column_count += 1;
			if ($column_count > 3) { 
				$column_count = 1;
			}
		?>

	<?php endforeach; ?>
	<div class="row">
		<div class="col-sm-12">

			<?php
				$pag = new Pagination($this->page, $this->pageCount, "?page=team&amp;p=");
				echo $pag->render();
			?>
<!--
 			<ul class="pager">
				<li><a href="?page=team&amp;p=<?php echo ($this->page - 1) ;?>">Previous</a></li>
				<li><a href="?page=team&amp;p=<?php echo ($this->page + 1) ;?>">Next</a></li>
			</ul>

			<ul class="pagination">
				<?php if ($this->page === 1): ?>
					<li class="disabled"><a href="#" title="Previous">&laquo;</a></li>
				<?php else: ?>
					<li><a href="?page=team&amp;p=<?php echo ($this->page - 1) ;?>" title="Previous">&raquo;</a></li>
				<?php endif; ?>

				<?php for ($i = 1; $i <= $this->pageCount; $i += 1): ?>
					<li
						<?php if ($i === $this->page): ?>
						class="active"
						<?php endif; ?>
					><a href="?page=team&amp;p=<?php echo $i; ?>"><?php echo $i; ?></a></li>
				<?php endfor; ?>

				<?php if ($this->page === $this->pageCount): ?>
					<li class="disabled"><a href="#" title="Next">&raquo;</a></li>
				<?php else: ?>
					<li><a href="?page=team&amp;p=<?php echo ($this->page + 1) ;?>" title="Next">&raquo;</a></li>
				<?php endif; ?>
				
			</ul> -->


		</div>
	</div>
</div>
<p><a href="?page=teammember&amp;action=add">
	<span class="glyphicon glyphicon-plus"></span> Add Team Member</a></p>



