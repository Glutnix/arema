    <form method="POST" action="./?page=login">

        <div class="form-group <?php $this->echoIfError('username', "has-error has-feedback"); ?>">
            <label for="username">Username</label>
            <input id="username" name="username" class="form-control"
                value="<?php $this->ee($this->data['username']); ?>"
            />
            <?php $this->showError('username'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password', "has-error has-feedback"); ?>">
            <label for="password">Password</label>
            <input id="password" name="password" type="password" class="form-control"
                value="<?php $this->ee($this->data['password']); ?>" />
            <?php $this->showError('password'); ?>
        </div>
        
    	<div class="form-group">
    		<button class="btn btn-primary">
                <span class="glyphicon glyphicon-user"></span> Log In
            </button>
    	</div>
    </form>
