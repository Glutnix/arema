<?php
    $url = "./?page=category";
    if ($this->data['id'] > 0) {
        $url .= "&amp;id=" . $this->data['id'];
    }
?>
    <form method="POST" action="<?php echo $url;?>">

        <div class="form-group <?php $this->echoIfError('name', "has-error has-feedback"); ?>">
            <label for="name">Category Name</label>
            <input id="name" name="name" class="form-control"
                    value="<?php $this->ee($this->data['name']); ?>" 
                    placeholder="e.g. Socks"
            />
            <?php $this->showError('name'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('description', "has-error has-feedback"); ?>">
            <label for="description">Description</label>
            <textarea id="description" name="description" class="form-control"><?php $this->ee($this->data['description']); ?></textarea>
            <?php $this->showError('description'); ?>
        </div>
        
    	<div class="form-group">
            
            <?php if ($this->data['id'] > 0): ?>
                <input type="hidden" name="action" value="edit" />
                <button class="btn btn-primary" type="submit">
                    <span class="glyphicon glyphicon-ok"></span>
                    Save Changes
                </button>
                <button class="btn btn-link pull-right" 
                    style="color: red;" type="submit" name="delete">
                    <span class="glyphicon glyphicon-trash"></span>
                    Delete
                </button>
            <?php else: ?>
                <input type="hidden" name="action" value="add" />
                <button class="btn btn-primary" type="submit">
                    <span class="glyphicon glyphicon-ok"></span>
                    Create Category
                </button>
            <?php endif; ?>

    	</div>
    </form>
