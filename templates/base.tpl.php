<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Arema – <?php $this->page_title(); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

    <div class="container">
        <div class="header">
            <ul class="nav nav-pills pull-right">
                <li <?php $this->echoIfSlug("home", 'class="active"'); ?>><a href="./">Home</a></li>
                <li <?php $this->echoIfSlug("categories", 'class="active"'); ?>><a href="./?page=categories">Products</a></li>
                <li <?php $this->echoIfSlug("about", 'class="active"'); ?>><a href="./?page=about">About</a></li>
                <li <?php $this->echoIfSlug("team", 'class="active"'); ?>><a href="./?page=team">Team</a></li>
                <li <?php $this->echoIfSlug("contact", 'class="active"'); ?>><a href="./?page=contact">Contact</a></li>
            </ul>
            <h3><a class="text-muted" href="./">Arema</a></h3>
        </div>

        <?php if (isset($_SESSION['flash'])): ?>
            <div class="alert alert-info" role="alert"><?php echo $_SESSION['flash']; ?></div>
        <?php endif; ?>

        <?php $this->page_contents(); ?>

        <div class="footer">
            <ul class="nav nav-pills pull-right">
                <?php if ($this->user->id > 0): ?>
                    <li><a href="./?page=profile"><?php echo $this->user->data['username']; ?></a></li>
                    <li><a href="./?page=logout">Logout</a></li>
                <?php else: ?>
                    <li><a href="./?page=register">Register</a></li>
                    <li><a href="./?page=login">Login</a></li>
                <?php endif; ?>
            </ul>
            <p>&copy; Arema 2014</p>
        </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
