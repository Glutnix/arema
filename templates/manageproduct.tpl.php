<?php
    $url = "./?page=product";
    if ($this->data['id'] > 0) {
        $url .= "&amp;id=" . $this->data['id'];
    }
?>
    <form method="POST" action="<?php echo $url;?>">

        <div class="form-group <?php $this->echoIfError('title', "has-error has-feedback"); ?>">
            <label for="title">Product Title</label>
            <input id="title" name="title" class="form-control"
                    value="<?php $this->ee($this->data['title']); ?>" 
                    placeholder="e.g. Red Socks"
            />
            <?php $this->showError('title'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('category_id', "has-error has-feedback"); ?>">
            <label for="category_id">Product Category</label>
            <select id="category_id" name="category_id" class="form-control">
                <?php $this->showSelectOptions($this->categories, $this->data['category_id']); ?>
            </select>
            <?php $this->showError('category_id'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('description', "has-error has-feedback"); ?>">
            <label for="description">Description</label>
            <textarea id="description" name="description" class="form-control"><?php $this->ee($this->data['description']); ?></textarea>
            <?php $this->showError('description'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('price', "has-error has-feedback"); ?>">
            <label for="price">Item Price</label>
            <input id="price" name="price" class="form-control"
                    value="<?php $this->ee($this->data['price']); ?>" 
                    placeholder="1.23"
            />
            <?php $this->showError('price'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('price_special', "has-error has-feedback"); ?>">
            <label for="price_special">Item Special Price</label>
            <input id="price_special" name="price_special" class="form-control"
                    value="<?php $this->ee($this->data['price_special']); ?>" 
                    placeholder="1.20"
            />
            <?php $this->showError('price_special'); ?>
        </div>
        
    	<div class="form-group">
            
            <?php if ($this->data['id'] > 0): ?>
                <input type="hidden" name="action" value="edit" />
                <button class="btn btn-primary" type="submit">
                    <span class="glyphicon glyphicon-ok"></span>
                    Save Changes
                </button>
                <button class="btn btn-link pull-right" 
                    style="color: red;" type="submit" name="delete">
                    <span class="glyphicon glyphicon-trash"></span>
                    Delete
                </button>
            <?php else: ?>
                <input type="hidden" name="action" value="add" />
                <button class="btn btn-primary" type="submit">
                    <span class="glyphicon glyphicon-ok"></span>
                    Create Product
                </button>
            <?php endif; ?>

    	</div>
    </form>
