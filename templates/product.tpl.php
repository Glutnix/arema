<ol class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li><a href="?page=categories">Products</a></li>
  <li><a href="?page=category&amp;id=<?php echo $this->data['category_id']; ?>"><?php $this->ee($this->model->category->data['name']); ?></a>
  <li class="active"><?php $this->ee($this->data['title']); ?></li>
</ol>

<h1><?php $this->ee($this->data['title']); ?></h1>
<p><?php $this->markdown($this->data['description']); ?></p>
<p>Price: $<?php echo $this->data['price']; ?></p>
<p>
	<a href="?page=product&amp;action=edit&amp;id=<?php $this->ee($this->data['id']); ?>">
	<span class="glyphicon glyphicon-pencil"></span> 
	Edit <?php $this->ee($this->data['title']); ?></a>
</p>
