<ol class="breadcrumb">
  <li><a href="./">Home</a></li>
  <li class="active">Products</li>
</ol>

<ul>
<?php foreach($this->products as $product): ?>
<li>
    <a href="?page=product&amp;id=<?php echo $product->data['id']; ?>">
        <?php $this->ee($product->data['title']); ?></a>
     - $<?php $this->ee($product->data['price']); ?>
</li>
<?php endforeach; ?>
</ul>
<p><a href="?page=product&amp;action=add">
	<span class="glyphicon glyphicon-plus"></span> Add Product</a></p>
