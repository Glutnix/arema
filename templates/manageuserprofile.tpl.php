    <form method="POST" action="./?page=profile" novalidate>

        <div class="form-group <?php $this->echoIfError('email', "has-error has-feedback"); ?>">
            <label for="email">Email Address</label>
            <input id="email" name="email" type="email" class="form-control"
                value="<?php $this->ee($this->data['email']); ?>" />
            <?php $this->showError('email'); ?>
        </div>

        <div class="form-group">
            <button class="btn btn-primary">
                <span class="glyphicon glyphicon-ok"></span> Update Email Address
            </button>
        </div>
    </form>

    <form method="POST" action="./?page=password">

        <div class="form-group <?php $this->echoIfError('oldpassword', "has-error has-feedback"); ?>">
            <label for="oldpassword">Current Password</label>
            <input id="oldpassword" name="oldpassword" type="password" class="form-control"
                value="" />
            <?php $this->showError('oldpassword'); ?>
        </div>


        <div class="form-group <?php $this->echoIfError('password', "has-error has-feedback"); ?>">
            <label for="password">New Password</label>
            <input id="password" name="password" type="password" class="form-control"
                value="" />
            <?php $this->showError('password'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password2', "has-error has-feedback"); ?>">
            <label for="password2">Confirm Password</label>
            <input id="password2" name="password2" type="password" class="form-control"
                value="" />
            <?php $this->showError('password2'); ?>
        </div>
        
        <div class="form-group">
            <button class="btn btn-primary">
                <span class="glyphicon glyphicon-ok"></span> Change Password
            </button>
        </div>
    </form>
