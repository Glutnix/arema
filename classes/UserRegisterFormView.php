<?php

class UserRegisterFormView extends FormView
{

	public $slug = "register";

	public function __construct ($model) {
		parent::__construct($this->slug, $model);
	}

	public function page_title () {
		echo "Register New User";
	}

}