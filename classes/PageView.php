<?php

abstract class PageView
{

    public $slug;

    protected $user;
    protected $flash;

    function __construct ($slug) {
        $this->slug = $slug;
        if ($this->slug === "" || !isset($this->slug)) {
            $this->slug = "home";
        }
    }

    public function renderTemplate ($slug) {
        $sub_template = "templates/" . $slug . ".tpl.php";
        if (!file_exists($sub_template)) {
            http_response_code(500);
            $error_message = "Page '<code>". $slug ."</code>' is an allowable page, but I couldn't find the template file.";
            include "templates/error.tpl.php";
        } else {
            include $sub_template;
        }
    }

    public function render () {
        include "templates/base.tpl.php";
        //$_SESSION['flash'] = null;
    }

    public function page_contents () {
        $this->renderTemplate($this->slug);
    }    

    public function echoIfSlug($slug, $str) {
        if ($slug === $this->slug) {
            echo $str;
        }
    }

    public function ee ($str) {
        echo htmlentities($str, null, 'UTF-8' );
    }

    public function markdown($str) {
        $str = htmlentities($str, null, 'UTF-8' );   
        $parsedown = new Parsedown();
        echo $parsedown->text($str);
    }

    public function plural ($count, $single, $plural) {
        if ( (int) $count === 1) {
            echo $single;
        } else {
            echo $plural;
        }
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function setFlash($flash) {
        $this->flash = $flash;
    }

}

