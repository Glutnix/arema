<?php

class ProductModel extends FormModel
{
	protected $db;
	public $id;
	public $data;
	public $category;

	protected $allowedFields = array( 'id', 'title', 'description', 'price', 'price_special', 'category_id');

	protected $validation = array(
		'title' => array('Validation::checkRequired', 'Validation::checkName'),
		'description' => array('Validation::checkRequired'),
		'price' => array('Validation::checkNumeric'),
		'price_special' => array('Validation::checkNumeric'),
		'category_id' => '*** SET IN CONSTRUCTOR ***'
	);


	public function __construct($db, $id = 0) {
		$this->validation['category_id'] = array( array($this, 'checkCategoryExists') );
		$this->db = $db;
		if (is_array($id)) {
			parent::__construct($id);
		} else { // is a number
			$this->processInput();
			$this->id = (int)$id;
			if ($id > 0) {
				$this->load($id);
			}
		}
	}

	public function processInput($input = null) {
		parent::processInput($input);
		if ($this->data['category_id'] > 0) {
			$this->category = new CategoryModel($this->db, $this->data['category_id']);
		}
	}

	public function commit() {
		if ($this->id > 0) {
			$this->update();
		} else {
			$this->create();
		}
	}

	protected function update() {
		// Do a database UPDATE.
		try {
			$statement = $this->db->prepare(
				"UPDATE products SET title = :title, description = :description, price = :price, price_special = :price_special, category_id = :category_id WHERE id = :id;"
			);
			$statement->bindValue(':title', $this->data['title']);
			$statement->bindValue(':description', $this->data['description']);
			$statement->bindValue(':price', $this->data['price']);
			$statement->bindValue(':price_special', $this->data['price_special']);
			$statement->bindValue(':category_id', $this->data['category_id']);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit;
		}	
	}

	protected function create() {
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare(
				"INSERT INTO products (title, description, price, price_special, category_id) VALUES (:title, :description, :price, :price_special, :category_id)"
			);
			$statement->bindValue(':title', $this->data['title']);
			$statement->bindValue(':description', $this->data['description']);
			$statement->bindValue(':price', $this->data['price']);
			$statement->bindValue(':price_special', $this->data['price_special']);
			$statement->bindValue(':category_id', $this->data['category_id']);
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}


	public function load($id) {
		$this->id = $id;
		try {
			$statement = $this->db->prepare("SELECT id, title, description, price, price_special, category_id FROM products WHERE id = :id;");
			$statement->bindValue(":id", $id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}
		$this->data = $result;
		$this->category = new CategoryModel($this->db, $this->data['category_id']);
	}

	public function remove() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM products WHERE id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}

	static public function fetchAll($db) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare("SELECT * FROM products");
			$statement->execute();
			
			// get the products record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->category = new CategoryModel($db, $record['category_id']);

				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function fetchAllByCategory($db, $category_id) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare("SELECT * FROM products WHERE category_id = :category_id");
			$statement->bindValue(":category_id", $category_id);
			$statement->execute();
			
			// get the products record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->category = new CategoryModel($db, $record['category_id']);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	public function checkCategoryExists($value) {
		return CategoryModel::checkValidCategory($this->db, $value);
	}


}