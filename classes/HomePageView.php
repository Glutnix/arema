<?php

class HomePageView extends PageView
{
	public $slug = "home";

	function __construct() {
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "Home";
	}

}