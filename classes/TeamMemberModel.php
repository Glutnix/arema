<?php

class TeamMemberModel extends FormModel
{

	protected $db;
	public $id;
	public $data;
	public $roles = array();

	protected $allowedFields = array( 'id', 'name', 'title', 'photo', 'roleid' );

	protected $validation = array(
		'name' => array('Validation::checkRequired', 'Validation::checkName'),
		'title' => array('Validation::checkRequired', 'Validation::checkName'),
		'photo' => array('TeamMemberModel::checkPhotoIsImageObject')
	);

	public function __construct($db, $id = 0) {
		$this->db = $db;
		if (is_array($id)) {
			parent::__construct($id);
		} else { // is a number
			$this->processInput();
			$this->id = (int)$id;
			if ($id > 0) {
				$this->load($id);
			} else {
				$this->data['photo'] = new Image();
			}
		}
	}

	public function commit() {
		if ($this->id > 0) {
			$this->update();
		} else {
			$this->create();
		}
	}

	protected function update() {
		// Do a database UPDATE.
		try {
			$statement = $this->db->prepare(
				"UPDATE team SET name = :name, title = :title, photo = :photo WHERE id = :id"
			);
			$statement->bindValue(':name', $this->data['name']);
			$statement->bindValue(':title', $this->data['title']);
			$statement->bindValue(':photo', basename($this->data['photo']->filename));
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

			$this->detachRoles();

			foreach($this->data['roleid'] as $role_id) {
				$role = new RoleModel($this->db, $role_id);
				$role->attach($this->id);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit;
		}	
	}

	protected function create() {
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare(
				"INSERT INTO team (name, title, photo) VALUES (:name, :title, :photo)"
			);
			$statement->bindValue(':name', $this->data['name']);
			$statement->bindValue(':title', $this->data['title']);
			$statement->bindValue(':photo', basename($this->data['photo']->filename));
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;

			foreach($this->data['roleid'] as $role_id) {
				$role = new RoleModel($this->db, $role_id);
				$role->attach($this->id);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}

	public function load($id) {
		$this->id = $id;
		try {
			$statement = $this->db->prepare(
				"SELECT * FROM team WHERE id = :id;"
			);
			$statement->bindValue(":id", $id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}
		$this->data = $result;
		$this->roles = RoleModel::fetchAllByTeamMember($this->db, $this->id);
		$this->processPhotoField();
	}

	protected function processPhotoField() {
		if (is_string($this->data['photo'])) {
			if ($this->data['photo'] !== "") {
				// convert to Image instance
				$img = new Image("img/team/" . $this->data['photo']);
			} else {
				$img = new Image();
			}
			if ($img->error === "") {
				$this->data['photo'] = $img;
			}
		}
	}

	public function remove() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM team WHERE id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}

	public function detachRoles() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM team_roles WHERE team_id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}


	static public function fetchAll($db) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare(
				"SELECT * FROM team"
			);
			$statement->execute();
			
			// get the team members record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function fetchLimit($db, $limit, $offset) {
		$result = array(
			'collection' => array(),
			'recordCount' => 0,
			'limit' => $limit,
			'offset' => $offset,
		);

		try {

			// get recordcount

			$statement = $db->prepare(
				"SELECT COUNT(id) FROM team;"
			);
			$statement->execute();
			$result['recordCount'] = (int) $statement->fetchColumn();

			// get records

			$statement = $db->prepare(
				"SELECT * FROM team LIMIT :limit OFFSET :offset;"
			);
			$statement->bindValue(":limit", $limit, PDO::PARAM_INT);
			$statement->bindValue(":offset", $offset, PDO::PARAM_INT);
			$statement->execute();

			// get the team members record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				array_push($result['collection'], $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $result;
	}


	static protected function checkPhotoIsImageObject($value) {
		// TODO
		// return true if value is an image and has no error!
		// else return the error or "not an image object"
		if (get_class($value) !== "Image" ) {
			return "Photo not uploaded.";
		}
		if ($value->filename !== "" && $value->error !== "") {
			return $value->error;
		}
		if (is_uploaded_file($value->filename)) {
			return "Photo not uploaded.";
		}
		return true;
	}

}