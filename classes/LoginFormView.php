<?php

class LoginFormView extends FormView
{
	public $slug = "login";

	public function __construct ($model) {
		parent::__construct($this->slug, $model);
	}

	public function page_title () {
		echo "Log In";
	}
}