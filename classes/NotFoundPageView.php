<?php

class NotFoundPageView extends PageView
{
	public $slug = "notfound";

	function __construct() {
		parent::__construct($this->slug);
		http_response_code(404);
	}	

	function page_title () {
		echo "404 Page Not Found";
	}

}