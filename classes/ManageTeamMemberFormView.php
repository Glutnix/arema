<?php

class ManageTeamMemberFormView extends FormView
{
	public $slug = "manageteammember";

	protected $roles;

	public function __construct($model, $rolesCollection) {
		$this->roles = $rolesCollection;
		parent::__construct($this->slug, $model);
	}

	public function page_title() {
		echo "Add New Team Member";
	}

}