<?php

class ManageProductFormView extends FormView
{
	public $slug = "manageproduct";
	protected $categories;

	public function __construct($model, $categories) {
		$this->categories = $categories;
		parent::__construct($this->slug, $model);
	}

	public function page_title() {
		echo "Add New Team Member";
	}

}