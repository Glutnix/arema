<?php

class AboutPageView extends PageView
{
	public $slug = "about";

	function __construct() {
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "About Us";
	}

}