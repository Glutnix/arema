<?php

class ContactFormView extends FormView
{

	public $slug = "contact";

	public function __construct ($input) {
		parent::__construct($this->slug, $input);
	}

	public function page_title () {
		echo "Contact Us";
	}

}