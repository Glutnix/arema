<?php

class ManageUserProfileFormView extends FormView
{
	public $slug = "manageuserprofile";

	public function __construct($model) {
		parent::__construct($this->slug, $model);
	}

	public function page_title() {
		echo "Update User Profile";
	}

}