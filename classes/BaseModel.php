<?php

abstract class BaseModel
{
	public $data;
	protected $input;
	public $errors;

	protected $allowedFields = array();

	public function processInput($input = null) {
		if (isset($input)) {
			$this->input = $input;
		}
		// check input property for allowed array indexes
		// if allowed, copy it to the data property.
		foreach ($this->allowedFields as $fieldName) {
			if (isset($this->input[$fieldName])) {
				//echo "found: " . $this->input[$fieldName]; 
				if ($fieldName === "id") {
					$this->id = $this->input[$fieldName];
				}
				if (is_string($this->input[$fieldName])) {
					$this->data[$fieldName] = trim($this->input[$fieldName]);
				} else {
					$this->data[$fieldName] = $this->input[$fieldName];
				}
			} else {
				// this field was not provided in input
				if (!isset($this->data[$fieldName])) {
					// make sure this field exists in data
					$this->data[$fieldName] = "";
				}
			}
			$this->errors[$fieldName] = ""; // prevent notices
		}
	}

}