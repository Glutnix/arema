<?php

class SingleCategoryPageView extends PageView
{
	public $slug = "category";
	protected $model; 
	protected $data;
	protected $products;

	function __construct($model) {
		$this->model = $model;
		$this->data = $this->model->data;
		$this->products = $this->model->getProducts();
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo $this->model->data['name'];
	}

}