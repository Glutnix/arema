<?php

class Pagination
{
	
	protected $url;
	protected $currentPage;
	protected $lastPage;
	protected $maxLinks;

	function __construct($theCurrentPage = 1, $theLastPage = 1, $theUrl = "?p=", $theMaxLinks = 10) {
		$this->currentPage = (int) $theCurrentPage;
		$this->lastPage = (int) $theLastPage;
		$this->maxLinks = (int) $theMaxLinks;
		$this->url = $theUrl;
	}


	function render() {
		$output = '<ul class="pagination">';

		// previous link
		$output .= $this->link($this->previousPage(), 'previous', '&laquo;', false);

		// first link
		$output .= $this->link(1, 'first');

		// calc intermediary links
		$range = $this->maxLinks - 2;
		$start = $this->currentPage - ( $range / 2 );

		if ($start + $range >= $this->lastPage) {
			$start = ($this->lastPage) - $range;
		}
		if ($start <= 2) {
			$start = 2;
		}

		$finish = $start + $range;
		if ($finish > $this->lastPage ) {
			$finish = $this->lastPage;
		}

		// output intermediary links
		$output .= $this->gap(1, $start);

		for($i = $start; $i < $finish; $i++) {
			$output .= $this->link($i);
		}
		$penultimateLink = $i - 1;
		
		// last link
		if ($penultimateLink !== $this->lastPage) {
			$output .= $this->gap($penultimateLink, $this->lastPage);
			$output .= $this->link($this->lastPage, 'last');
		}
		// next
		$output .= $this->link($this->nextPage(), 'next', '&raquo;', false);


		$output .= '</ul>';
		return $output;
	}

	// Used by paginationHTML below...
	protected function link($page, $class = "", $theLabel = false, $showActive = true) {
		if (!$theLabel) {
			$label = $page;
		} else {
			$label = $theLabel;
		}
		$active = "";
		$url = $this->url . $page;
		if ($page == $this->currentPage) {
			$url = "#";
			if ($showActive) {
				$active = " class='active'";
			} else {
				$active = " class='disabled'";
			}
		}
		return '<li ' . $active . '><a href="' . $url . '" class="' . $class . '">' . $label . '</a></li>';
	}


	// Used by paginationHTML below...
	protected function gap($p1, $p2) {
		$x = max($p1, $p2) - min($p1, $p2);
		if ($x <= 1) {
			return  '';
		}
		$output = "<li class='disabled'><a href='#'> .. </a></li>";
		return $output;
	}

	protected function previousPage() {
		if ($this->currentPage == 1) return 1;
		return $this->currentPage - 1;
	}

	protected function nextPage() {
		if ($this->currentPage == $this->lastPage) return $this->lastPage;
		return $this->currentPage + 1;
	}

}
