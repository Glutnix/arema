<?php

class TeamPageView extends PageView
{
	public $slug = "team";
	protected $team;

	function __construct($result) {
		$this->team = $result['collection'];
		
		$limit = $result['limit'];
		$offset = $result['offset'];

		$this->page = ($offset / $limit) + 1;
		$this->pageCount = (int) ceil($result['recordCount'] / $limit); 

		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "Our Team";
	}

	public function getPhotoElement($member) {
		if ($member->data['photo']->filename == "") {
			// no image, use a placeholder
			echo '<img src="http://placehold.it/203x203" alt="" width="203" />';
		} else if ($member->data['photo']->mimetype === "image/gif") {
			// don't give a resized thumb for gifs
			echo '<img src="' . $member->data['photo']->filename . '" alt="" width="203" />';
		} else {
			// show resized thumbnail
			echo '<img src="' . $member->data['photo']->getThumbnail(203, 203)->filename . '" alt="" width="203" />';
		}
	}

}