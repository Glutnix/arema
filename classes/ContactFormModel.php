<?php

class ContactFormModel extends FormModel
{
	protected $sendToEmail = "brett@webfroot.co.nz";
	protected $subjectLine = "Arema Contact Form";

	protected $allowedFields = array("fullname", "email", "message");

	protected $validation = array(
		'fullname' => array('Validation::checkRequired', 'Validation::checkName'),
		'email' => array('Validation::checkRequired', 'Validation::checkEmail'),
		'message' => array('Validation::checkMessage')
	);

	protected function commit() {
		$message = "Got a message for you from ";
		$message .= htmlentities($this->data['fullname'], null, 'UTF-8') . "\n";
		$message .= htmlentities($this->data['email'], null, 'UTF-8') . "\n";
		$message .= "\n";
		$message .= htmlentities($this->data['message'], null, 'UTF-8') . "\n";
		$message .= "\n";

		if (HOST_TYPE === "live") {
			mail($this->sendToEmail, $this->subjectLine, $message);
		} else {
			$this->debugOutput = $message;
		}
	}

}