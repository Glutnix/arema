<?php

class SingleTeamMemberPageView extends PageView
{
	public $slug = "teammember";
	protected $model; 
	protected $data;

	function __construct($model) {
		$this->model = $model;
		$this->data = $this->model->data;
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo $this->model->data['name'];
	}

	public function getPhotoElement() {
		if ($this->data['photo']->filename == "") {
			// no image, use a placeholder
			echo '<img src="http://placehold.it/350x350" alt="" width="350" />';
		} else if ($this->data['photo']->mimetype === "image/gif") {
			// don't give a resized thumb for gifs
			echo '<img src="' . $this->data['photo']->filename . '" alt="" width="350" />';
		} else {
			// show resized thumbnail
			echo '<img src="' . $this->data['photo']->getThumbnail(350, 350)->filename . '" alt="" width="305" />';
		}
	}

}