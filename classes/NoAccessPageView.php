<?php

class NoAccessPageView extends PageView
{
	public $slug = "noaccess";

	function __construct() {
		parent::__construct($this->slug);
		http_response_code(403);
	}	

	function page_title () {
		echo "403 Forbidden";
	}

}