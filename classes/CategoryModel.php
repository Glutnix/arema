<?php

class CategoryModel extends FormModel
{
	protected $db;
	public $id;
	public $data;

	protected $allowedFields = array( 'id', 'name', 'description', 'productcount');

	protected $validation = array(
		'name' => array('Validation::checkRequired', 'Validation::checkName'),
		'description' => array('Validation::checkRequired'),
	);


	public function __construct($db, $id = 0) {
		$this->db = $db;
		if (is_array($id)) {
			parent::__construct($id);
		} else { // is a number
			$this->processInput();
			$this->id = (int)$id;
			if ($id > 0) {
				$this->load($id);
			}
		}
	}

	public function commit() {
		if ($this->id > 0) {
			$this->update();
		} else {
			$this->create();
		}
	}

	protected function update() {
		// Do a database UPDATE.
		try {
			$statement = $this->db->prepare(
				"UPDATE categories SET name = :name, description = :description WHERE id = :id;"
			);
			$statement->bindValue(':name', $this->data['name']);
			$statement->bindValue(':description', $this->data['description']);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit;
		}	
	}

	protected function create() {
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare(
				"INSERT INTO categories (name, description) VALUES (:name, :description)"
			);
			$statement->bindValue(':name', $this->data['name']);
			$statement->bindValue(':description', $this->data['description']);
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}


	public function load($id) {
		$this->id = $id;
		try {
			$statement = $this->db->prepare("SELECT id, name, description FROM categories WHERE id = :id;");
			$statement->bindValue(":id", $id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}
		$this->data = $result;
	}

	public function remove() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM categories WHERE id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}

	public function getProducts () {
		return ProductModel::fetchAllByCategory($this->db, $this->id);
	}


	static public function fetchAll($db) {
		$collection = array();

		// get all of the things from the database.
		try {
			// heredoc syntax looks ugly but helps syntax highlighting
			$sql = <<<SQL
				SELECT c.id, 
				       c.name, 
				       c.description, 
				       COUNT(p.category_id) AS productcount 
				FROM   categories AS c 
				       LEFT OUTER JOIN products AS p 
				              ON c.id = p.category_id 
				GROUP BY c.id;			
SQL;
			$statement = $db->prepare( $sql );
			$statement->execute();
			
			// get the products record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function checkValidCategory($db, $id) {
		if ($id <= 0) {
			return "Category must be positive.";
		}
		$category = new self($db, $id);
		if ($category->id === 0) {
			return "Category does not exist.";
		}
		return true;

	}

}