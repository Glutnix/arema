<?php

class ProductsPageView extends PageView
{
	public $slug = "products";
	protected $products;

	function __construct($collection) {
		$this->products = $collection;
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "Our Products";
	}

}