<?php

class RoleModel extends FormModel
{

	protected $db;
	public $id;
	public $data;

	protected $allowedFields = array( 'id', 'rolename');

	protected $validation = array(
		'rolename' => array('Validation::checkRequired', 'Validation::checkName')
	);

	public function __construct($db, $id = 0) {
		$this->db = $db;
		if (is_array($id)) {
			parent::__construct($id);
		} else { // is a number
			$this->processInput();
			$this->id = (int)$id;
			if ($id > 0) {
				$this->load($id);
			}
		}
	}

	public function commit() {
		if ($this->id > 0) {
			$this->update();
		} else {
			$this->create();
		}
	}

	protected function update() {
		// Do a database UPDATE.
		try {
			$statement = $this->db->prepare(
				"UPDATE roles SET rolename = :rolename WHERE id = :id"
			);
			$statement->bindValue(':rolename', $this->data['rolename']);
;
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit;
		}	
	}

	protected function create() {
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare(
				"INSERT INTO roles (rolename) VALUES (:rolename)"
			);
			$statement->bindValue(':rolename', $this->data['rolename']);
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}

	public function load($id) {
		$this->id = $id;
		try {
			$statement = $this->db->prepare(
				"SELECT * FROM roles WHERE id = :id;"
			);
			$statement->bindValue(":id", $id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}
		$this->data = $result;
	}

	public function remove() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM roles WHERE id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}

	public function attach( $team_id ) {
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare( <<<"SQL"
				INSERT team_roles (team_id, role_id) VALUES (:team_id, :role_id);
SQL
			);
			$statement->bindValue(':team_id', (int)$team_id);
			$statement->bindValue(':role_id', $this->id);
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}

	static public function fetchAll($db) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare(
				"SELECT * FROM roles"
			);
			$statement->execute();
			
			// get the team members record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function fetchAllByTeamMember($db, $id) {
			$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare( <<<"SQL"
				SELECT r.id,
				       rolename
				FROM team_roles AS tr
				LEFT JOIN roles AS r ON tr.role_id = r.id
				WHERE team_id = :id
SQL
			);
			$statement->bindValue(':id', (int) $id);
			$statement->execute();
			
			// get the team members record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

}