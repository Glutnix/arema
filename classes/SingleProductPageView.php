<?php

class SingleProductPageView extends PageView
{
	public $slug = "product";
	protected $model; 
	protected $data;

	function __construct($model) {
		$this->model = $model;
		$this->data = $this->model->data;
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo $this->model->data['title'];
	}

}