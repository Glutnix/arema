<?php

class ManageCategoryFormView extends FormView
{
	public $slug = "managecategory";

	public function __construct($model) {
		parent::__construct($this->slug, $model);
	}

	public function page_title() {
		echo "Manage Category";
	}

}