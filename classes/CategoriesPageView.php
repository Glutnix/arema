<?php

class CategoriesPageView extends PageView
{
	public $slug = "categories";
	protected $categories;

	function __construct($collection) {
		$this->categories = $collection;
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "Our Products";
	}

}