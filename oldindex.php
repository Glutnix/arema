<?php
require "lib/http_response_code.inc.php";

$page = $_GET['page'];

$allowable_pages = array('home','products','contact');

// default to 'home'
if (!isset($page) || $page === "") {
	$page = "home";
}

// send to 404 if page not in whitelist.
if (!in_array($page, $allowable_pages)) {
	http_response_code(404);
	$page = "notfound";
}


$template_path = "templates/"; // trailing slash!
$base_template = $template_path . "base.tpl.php";
$sub_template = $template_path . $page . ".tpl.php";

if (file_exists($sub_template)) {
	include $sub_template;
} else {
	http_response_code(500);
	function error_message () {
		global $page;
		?>
		Page '<code><?php echo $page; ?></code>' is an allowable page, but I couldn't find the template file.
		<?php
	}
	include $template_path . "error.tpl.php";
}

include $base_template;
